const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

function updateSearches(addedBy, cb) {
    const batch = admin.firestore().batch();
    const promiseArr = addedBy.map(friend => {
        return admin
            .firestore()
            .doc(`users/${friend}`)
            .get()
            .then(snap => {
                const { searches } = snap.data();
                return batch.update(snap.ref, {
                    searches: cb(searches)
                });
            });
    });
    return Promise.all(promiseArr).then(() => batch.commit());
}

exports.getSuggestions = functions.https.onCall((data, context) =>
    admin
        .firestore()
        .collection('items')
        .get()
        .then(querySnapshot => {
            const items = [];
            querySnapshot.forEach(doc =>
                items.push(Object.assign({ id: doc.id }, doc.data()))
            );
            const regex = new RegExp(data.search, 'i');
            const matchedItems = items.filter(item => regex.test(item.title));
            return { items: matchedItems };
        })
);

exports.reviewHandler = functions.firestore
    .document('reviews/{pushId}')
    .onCreate((snap, context) => {
        const review = snap.data();
        admin
            .firestore()
            .doc(`deals/${review.deal}`)
            .update({ isRated: true });
        const userReviewsCount = Number(review.prevUserReviewsCount) + 1;
        const itemReviewsCount = Number(review.prevItemReviewsCount) + 1;
        const userSummaryRating = Number(review.prevUserRating) + review.userRating;
        const itemSummaryRating = Number(review.prevItemRating) + review.itemRating;
        return Promise.all([
            admin
                .firestore()
                .doc(`users/${review.to}`)
                .update({
                    rating: userSummaryRating / userReviewsCount,
                    reviewsCount: userReviewsCount
                }),
            admin
                .firestore()
                .doc(`items/${review.item}`)
                .update({
                    rating: itemSummaryRating / itemReviewsCount,
                    reviewsCount: itemReviewsCount
                })
        ]);
    });

exports.usersHandler = functions.firestore
    .document('users/{id}')
    .onCreate((snap, context) => {
        const { friends } = snap.data();
        return admin
            .firestore()
            .doc(`users/${friends[0]}`)
            .get()
            .then(snap => {
                const { addedBy } = snap.data();
                const updated = addedBy.concat([context.params.id]);
                return admin
                    .firestore()
                    .doc(`users/${friends[0]}`)
                    .update({ addedBy: updated });
            });
    });

exports.createSearchHandler = functions.firestore
    .document('requests/{id}')
    .onCreate((snap, context) => {
        const { type = '', from: user = {} } = snap.exists && snap.data();
        return admin
            .firestore()
            .doc(`users/${user}`)
            .get()
            .then(snap => {
                const { addedBy, email } = snap.data();
                if (type === 'search') {
                    return updateSearches(addedBy, arr =>
                        arr.concat([context.params.id])
                    );
                } else return false;
            });
    });

exports.updateSearchHandler = functions.firestore
    .document('requests/{id}')
    .onUpdate((change, context) => {
        // handle Status
        const { status, item, from, owner, date } = change.after.data();
        const itemRef = admin.firestore().doc(`items/${item}`);
        if (status === 'inUse') itemRef.update({ status: 'inUse' });
        else itemRef.update({ status: 'active' });
        if (status === 'finished')
            admin
                .firestore()
                .collection(`deals`)
                .add({
                    from: owner,
                    to: from,
                    item,
                    startDate: date,
                    endDate: Date.now()
                });
        // handle search
        const typeBefore = change.before.exists && change.before.data().type;
        const { type: typeAfter, from: user } =
            change.after.exists && change.after.data();
        if (typeAfter === 'request' && typeBefore === 'search') {
            return admin
                .firestore()
                .doc(`users/${user}`)
                .get()
                .then(snap => {
                    const { addedBy } = snap.data();
                    return updateSearches(addedBy, arr =>
                        arr.filter(search => search !== context.params.id)
                    );
                });
        } else return false;
    });

exports.updateFriendsHandler = functions.firestore
    .document('users/{id}')
    .onUpdate((change, context) => {
        const friendsBefore = change.before.get('friends');
        const friendsAfter = change.after.get('friends');
        let addedBy, ref;
        if (friendsBefore.length > friendsAfter.length) {
            const deletedUser = friendsBefore.find(
                user => !friendsAfter.includes(user)
            );
            return admin
                .firestore()
                .doc(`users/${deletedUser}`)
                .get()
                .then(snap => {
                    ref = snap.ref;
                    addedBy = snap
                        .get('addedBy')
                        .filter(userId => userId !== context.params.id);
                    return admin
                        .firestore()
                        .collection('requests')
                        .where('type', '==', 'search')
                        .where('from', '==', deletedUser)
                        .get();
                })
                .then(snap => {
                    const searchIds = snap.empty
                        ? []
                        : snap.docs.map(doc => doc.id);
                    return ref.update({ addedBy }).then(() => {
                        return updateSearches([context.params.id], arr =>
                            arr.filter(search => !searchIds.includes(search))
                        );
                    });
                });
        } else if (friendsBefore.length < friendsAfter.length) {
            const addedUser = friendsAfter.find(
                user => !friendsBefore.includes(user)
            );
            return admin
                .firestore()
                .doc(`users/${addedUser}`)
                .get()
                .then(snap => {
                    ref = snap.ref;
                    addedBy = snap.get('addedBy').concat([context.params.id]);
                    return admin
                        .firestore()
                        .collection('requests')
                        .where('type', '==', 'search')
                        .where('from', '==', addedUser)
                        .get();
                })
                .then(snap => {
                    const searchIds = snap.empty
                        ? []
                        : snap.docs.map(doc => doc.id);
                    return ref.update({ addedBy }).then(() => {
                        return updateSearches(addedBy, arr =>
                            arr.concat(searchIds)
                        );
                    });
                });
        } else return false;
    });
