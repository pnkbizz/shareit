require('dotenv').config();
const express = require('express');
const passport = require('./server/passport');
const router = require('./server/router');
const history = require('connect-history-api-fallback');
const cookieParser = require('cookie-parser');

const app = express();

app.use(cookieParser(process.env.JWT_SECRET));
app.use(express.json());
app.use(passport.initialize());
app.use('/', router);
app.use(history());

app.use(express.static(`${__dirname}/public`));

app.get('*.js', (req, res, next) => {
    req.url += '.gz';
    res.set('Content-Encoding', 'gzip');
    next();
});

app.get('*', (req, res) => {
    res.sendFile(`${__dirname}/public/index.html`);
});

app.listen(process.env.PORT || 4003);
