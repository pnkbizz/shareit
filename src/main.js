import Vue from 'vue';
import Vuefire from 'vuefire';
import VueMaterial from 'vue-material';
import { sync } from 'vuex-router-sync';
import VueTouch from 'vue-touch';

import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

import App from './App.vue';
import firebase from './firebase';
import router from './router';
import store from './store';

window.vkAsyncInit = () => {
    // eslint-disable-next-line no-undef
    VK.init({
        apiId: 6323174
    });
};

setTimeout(() => {
    const el = document.createElement('script');
    el.type = 'text/javascript';
    el.src = 'https://vk.com/js/api/openapi.js?152';
    el.async = true;
    document.body.appendChild(el);
}, 0);

Vue.use(VueMaterial);
Vue.use(Vuefire);
Vue.use(VueTouch, { name: 'v-touch' });
sync(store, router);

Vue.config.productionTip = false;

let app;

Vue.prototype.$auth = firebase.auth();
Vue.prototype.$db = firebase.firestore();
firebase.auth().onAuthStateChanged(user => {
    if (!app) {
        app = new Vue({
            el: '#app',
            router,
            store,
            render: h => h(App)
        });
    }
});
