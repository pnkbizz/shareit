import Vue from 'vue';
import Router from 'vue-router';
import firebase from './firebase';
import store from './store';

const Login = () => import('./pages/login.vue');
const Profile = () => import('./pages/userProfile.vue');
const Details = () => import('./pages/mainProfileDetails.vue');
const Items = () => import('./pages/items.vue');
const showItems = () => import('./pages/showItems.vue');
const itemDetails = () => import('./pages/itemDetails.vue');
const addItem = () => import('./pages/addItem.vue');
const showRequests = () => import('./pages/showRequests.vue');
const addRequest = () => import('./pages/addRequest.vue');
const AccessDenied = () => import('./pages/AccessDenied.vue');
const About = () => import('./pages/about.vue');
const StreamPage = () => import('./pages/stream.vue');

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '*',
            redirect: '/login'
        },
        {
            path: '/',
            name: 'Main',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/requests',
            name: 'Requests',
            component: Items,
            redirect: '/requests/in',
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: 'in',
                    name: 'incoming',
                    meta: { status: 'incoming' },
                    component: showRequests,
                    props: { isCurrentUser: false }
                },
                {
                    path: 'out',
                    name: 'outgoing',
                    meta: { status: 'outgoing' },
                    component: showRequests
                },
                {
                    path: 'done',
                    name: 'finished',
                    meta: { status: 'finished' },
                    component: showRequests
                },
                {
                    path: 'add',
                    name: 'Add Request',
                    component: addRequest,
                    meta: { tabs: false }
                }
            ]
        },
        {
            path: '/items',
            name: 'Items',
            component: Items,
            redirect: '/items/active',
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: 'active',
                    name: 'active',
                    meta: { status: 'active' },
                    component: showItems
                },
                {
                    path: 'in-use',
                    name: 'inUse',
                    meta: { status: 'inUse' },
                    component: showItems
                },
                {
                    path: 'archived',
                    name: 'archived',
                    meta: { status: 'archived' },
                    component: showItems
                },
                {
                    path: 'add',
                    name: 'Add Items',
                    component: addItem,
                    meta: { tabs: false }
                },
                {
                    path: ':itemKey',
                    name: 'Item Details',
                    component: itemDetails,
                    props: true,
                    meta: { tabs: false }
                }
            ]
        },
        {
            path: '/profile/:id',
            redirect: '/profile/:id/items',
            name: 'Profile',
            component: Profile,
            props: true,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: 'items',
                    name: 'profileItems',
                    component: showItems,
                    props: { isCurrentUser: false }
                },
                {
                    path: 'requests',
                    name: 'profileRequests',
                    component: showRequests,
                    props: {
                        showedTypes: ['search'],
                        isCurrentUser: false
                    }
                }
            ]
        },
        {
            path: '/stream',
            name: 'stream',
            component: StreamPage,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/details',
            name: 'Details',
            component: Details,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/about',
            name: 'About',
            component: About,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/403',
            name: '403',
            component: AccessDenied
        }
    ]
});

router.beforeEach((to, from, next) => {
    store.commit('setField', { field: 'isRouteLoading', value: true });
    const { currentUser } = firebase.auth();
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) next('/login');
    else if (currentUser && to.name === 'Login') next('/stream');
    else next();
});

router.beforeResolve((to, from, next) => {
    store.commit('setField', { field: 'isRouteLoading', value: false });
    next();
});

export default router;
