import {
    MdToolbar,
    MdButton,
    MdIcon,
    MdSnackbar,
    MdDrawer,
    MdList,
    MdDivider,
    MdAvatar,
    MdCard,
    MdField,
    MdAutocomplete,
    MdDialog,
    MdDialogConfirm,
    MdProgress,
    MdTabs,
    MdRadio,
    MdSubheader
} from 'vue-material/dist/components';
import Vue from 'vue';

Vue.use(MdToolbar);
Vue.use(MdButton);
Vue.use(MdIcon);
Vue.use(MdSnackbar);
Vue.use(MdDrawer);
Vue.use(MdList);
Vue.use(MdDivider);
Vue.use(MdAvatar);
Vue.use(MdCard);
Vue.use(MdField);
Vue.use(MdAutocomplete);
Vue.use(MdDialog);
Vue.use(MdDialogConfirm);
Vue.use(MdProgress);
Vue.use(MdTabs);
Vue.use(MdRadio);
Vue.use(MdSubheader);
