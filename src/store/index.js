import Vue from 'vue';
import Vuex from 'vuex';
import { firebaseMutations, firebaseAction } from 'vuexfire';

import user from './user';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: { user },
    state: {
        detailedUserItems: [],
        categories: [],
        detailedUserRequests: [],
        currentItem: {},
        ownerDetails: {},
        snackbarMessage: '',
        showSnackbar: false,
        showDrawer: false,
        device: '',
        isRouteLoading: false
    },
    getters: {
        detailedUserSearches(state) {
            return state.detailedUserRequests.filter(
                req => req.type === 'search'
            );
        },
        isMobileDevice(state) {
            return state.device !== 'desktop';
        }
    },
    mutations: {
        setField(state, { field, value }) {
            // eslint-disable-next-line no-param-reassign
            state[field] = value;
        },
        setSnackbarMessage(state, msg) {
            // eslint-disable-next-line no-param-reassign
            state.snackbarMessage = msg;
            // eslint-disable-next-line no-param-reassign
            state.showSnackbar = true;
        },
        ...firebaseMutations
    },
    actions: {
        bindRef: firebaseAction(
            ({ bindFirebaseRef }, { field, refObj: ref }) => {
                bindFirebaseRef(field, ref);
            }
        ),
        unbindRef: firebaseAction(({ unbindFirebaseRef }, fields) => {
            if (Array.isArray(fields)) {
                fields.forEach(field => unbindFirebaseRef(field));
            } else unbindFirebaseRef(fields);
        })
    }
});
