import { firebaseAction } from 'vuexfire';
import { format } from 'date-fns';
import ru from 'date-fns/locale/ru';

const statuses = [
    'active',
    'inUse',
    'archived',
    'incoming',
    'outgoing',
    'finished'
];

function getDate(date) {
    console.log('1111');
    return Number(format(new Date(date), 'x', {
        locale: ru 
    }));
}

export default {
    namespaced: true,
    state: {
        items: [],
        incomingRequests: [],
        outgoingRequests: [],
        lastFiveItemsBefore: [],
        lastFiveItemsAfter: [],
        lastFiveSearchesBefore: [],
        lastFiveSearchesAfter: [],
        incomingDeals: [],
        outgoingDeals: [],
        ratedDeals: [],
        searches: [],
        profile: {},
        currentDealKey: ''
    },
    getters: {
        showedStatus(state, getters, rootState) {
            return statuses.includes(rootState.route.meta.status)
                ? rootState.route.meta.status
                : null;
        },
        requests(state, getters) {
            return getters[`${getters.showedStatus}Requests`] || [];
        },
        searches(state, getters) {
            return getters[`${getters.showedStatus}Searches`] || [];
        },
        incomingRequests(state) {
            return state.incomingRequests.filter(req => !['finished', 'declined'].includes(req.status));
        },
        outgoingRequests(state) {
            return (
                (state.outgoingRequests &&
                    state.outgoingRequests.filter(req =>
                        req.type === 'request' && !['finished', 'declined'].includes(req.status))) ||
                []
            );
        },
        finishedRequests(state, getters) {
            return (
                state.outgoingRequests &&
                state.outgoingRequests
                    .concat(state.incomingRequests || [])
                    .filter(req => ['finished', 'declined'].includes(req.status))
            );
        },
        incomingSearches(state) {
            return state.profile && state.profile.searches;
        },
        outgoingSearches(state, getters) {
            return (
                (state.outgoingRequests &&
                    state.outgoingRequests.filter(req => req.type === 'search')) ||
                []
            );
        },
        items(state, getters) {
            return (
                state.items &&
                state.items.filter(item => item.status === getters.showedStatus)
            );
        },
        firstFive(state) {
            return state.items && [...state.items].reverse().slice(0, 5);
        },
        key(state) {
            return state.profile && state.profile.id;
        },
        currentDealStatus(state) {
            return state.incomingDeals.findIndex(deal => deal.id === state.currentDealKey) !== -1
                ? 'incoming'
                : 'outgoing';
        },
        lastFiveItems(state) {
            return state.lastFiveItemsAfter
                .concat(state.lastFiveItemsBefore)
                .sort((a, b) => {
                    console.log(a, b);
                    return a.created - b.created
                })
                .slice(0, 5)
        },
        lastFiveSearches(state) {
            return state.lastFiveSearchesAfter
                .concat(state.lastFiveSearchesBefore)
                .sort((a, b) => getDate(a.date) - getDate(b.date))
                .slice(0, 5)
        },
    },
    mutations: {
        setField(state, { field, value }) {
            // eslint-disable-next-line no-param-reassign
            state[field] = value;
        }
    },
    actions: {
        bindRef: firebaseAction(({ bindFirebaseRef }, { field, refObj: ref }) => {
            bindFirebaseRef(field, ref);
        })
    }
};
