import { format } from 'date-fns';
import ru from 'date-fns/locale/ru';

export default {
    props: ['request', 'isMobile'],
    computed: {
        date() {
            return (
                this.request &&
                format(new Date(this.request.date), 'DD MMMM HH:MM', {
                    locale: ru
                })
            );
        }
    }
};
