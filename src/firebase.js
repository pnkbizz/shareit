import Firebase from 'firebase';

import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/functions';

const config = {
    apiKey: 'AIzaSyDkFzQGfGWB3lhS53IR53YhdEDWjoBRjuU',
    authDomain: 'communa-project.firebaseapp.com',
    databaseURL: 'https://communa-project.firebaseio.com',
    projectId: 'communa-project',
    storageBucket: 'communa-project.appspot.com',
    messagingSenderId: '228454782867'
};

const fb = Firebase.initializeApp(config);
fb.auth().languageCode = 'ru';
const db = fb.firestore();
db.settings({ timestampsInSnapshots: true });
const functions = fb.functions();

function sendRequest({ itemKey, userKey, owner }) {
    return db.collection('requests').add({
        item: itemKey,
        from: userKey,
        owner,
        date: Date.now(),
        status: 'created',
        type: 'request'
    });
}

function updateSearch(key) {
    return db
        .doc(`requests/${key}`)
        .get()
        .then((snap) => {
            const updatedSearch = Object.assign({ id: snap.id }, snap.data());
            this.mappedSearches = this.mappedSearches
                .map(search => (search.id === updatedSearch.id ? updatedSearch : search));
        });
}

export { sendRequest, updateSearch, Firebase, functions };
export default fb;
