import Drawer from '../drawer.vue';
import VueMaterial from 'vue-material';
import { mount, createLocalVue } from 'vue-test-utils'

describe('Компонент Drawer', () => {
    const localVue = createLocalVue();
    localVue.use(VueMaterial);

    const currentUser = {
        '.key':"EEREmED1blXatfm3wwQ6VLw9IpG3",
        displayName:"Павел Галкин",
        email:"bizz.gc@yandex.ru",
        gender:"male",
        id:2465473,
        incomingDeals:0,
        invitesCount:9,
        outgoingDeals:0,
        phone:"+79206327889",
        photos: [
            {
                type:"photo",
                value:"https://pp.userapi.com/c627223/v627223473/183d1/p9F8Btj6UYY.jpg"
            },
            {
                type:"photo_400_orig",
                value:"https://pp.userapi.com/c627223/v627223473/183ce/CoAvDOQAMuc.jpg"
            }
        ],
        profileUrl:"http://vk.com/fotobiz",
        provider:"vkontakte",
        rating:0,
        username:"fotobiz"
    }
    const wrapper = mount(Drawer, {
        localVue,
        propsData: {
            isShowed: true,
            currentUser
        }
    })
  
    it('Проверка видимости', () => {
      expect(wrapper.vm.showDrawer).toBe(true)
    })

    it('Проверка скрытия', () => {
        wrapper.setData({ showDrawer: false })
        expect(wrapper.classes()).toContain('md-drawer')
    })
})