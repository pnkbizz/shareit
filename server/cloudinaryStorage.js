class CloudinaryStorage {
    constructor(cloudinary, logger = {}) {
        this.cloudinary = cloudinary;
        this.logger = logger;
    }

    _handleFile(req, file, cb) {
        const params = {
            crop: 'limit',
            width: 1600,
            height: 1600,
            eager: {
                crop: 'fill',
                width: 450,
                height: 300
            }
        };
        const stream = this.cloudinary.v2.uploader.upload_stream(params, cb);

        file.stream.pipe(stream);
    }

    _removeFile(req, file, cb) {
        this.cloudinary.v2.uploader.destroy(
            file.file_id,
            { invalidate: true },
            cb
        );
    }
}

module.exports = function cloudinaryStorage({ cloudinary } = {}) {
    return new CloudinaryStorage(cloudinary);
};
