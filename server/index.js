require('dotenv').config();
const express = require('express');
const passport = require('./passport');
const router = require('./router');
const history = require('connect-history-api-fallback');
const cookieParser = require('cookie-parser');

const app = express();

app.use(cookieParser(process.env.JWT_SECRET));
app.use(express.json());
app.use(passport.initialize());
app.use('/', router);
app.use(history({
    index: '/public/'
}));

app.use(express.static('/public/'));
app.listen(process.env.PORT || 4003);
