const express = require('express');
const multer = require('multer');
const cloudinary = require('cloudinary');
const jwt = require('jsonwebtoken');
const axios = require('axios');

const transporter = require('./mailer');
const passport = require('./passport');
const { auth } = require('./firebase');
const firestore = require('./firestore');

cloudinary.config({
    /* eslint-disable */
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
    /* eslint-enable */
});

const storage = require('./cloudinaryStorage')({ cloudinary });

const upload = multer({ storage });

const router = express.Router();

function getUserToken(uid, res) {
    return auth
        .createCustomToken(uid)
        .then(customToken =>
            res.redirect(`${process.env.BASE_URL}/login?token=${customToken}`))
        .catch(err => res.status(500).send(err));
}

function getBotMessage({ item, user }) {
    const root = 'http://www.communa.me';
    const isItem = item.type === 'item';
    const typeName = isItem ? 'предложение' : 'запрос';
    const url = isItem
        ? `${root}/items/${item.id}`
        : `${root}/profile/${user.id}/requests/`;
    return `[${user.name}](${root}/profile/${user.id}) добавил *${typeName}*
[${item.title}](${url})
_${item.description}_`;
}

router.get(
    '/api/auth/vkontakte',
    passport.authenticate('vkontakte', { display: 'popup' })
);

router.get(
    '/api/auth/vkontakte/callback',
    passport.authenticate('vkontakte', {
        failureRedirect: `${process.env.BASE_URL}/login`
    }),
    async(req, res) => {
        const { uid } = req.user;
        try {
            await auth.getUser(uid);
            return getUserToken(uid, res);
        } catch (err) {
            if (err.errorInfo.code !== 'auth/user-not-found') {
                return res.status(500).send(err);
            }
            try {
                const cookie = JSON.parse(req.signedCookies.tmp_sgn);
                const { sender, email } = cookie;
                if (!sender) return res.redirect(`${process.env.BASE_URL}/403`);
                const inviterRef = await firestore.doc(`users/${sender}`).get();
                const { invitesCount } = inviterRef.data();
                if (invitesCount <= 0) {
                    res.redirect(`${process.env.BASE_URL}/403`);
                }
                const user = await firestore.doc(`tmpUsers/${uid}`).get();
                const fullProfile = Object.assign(
                    { email, invitedBy: sender, friends: [sender] },
                    user.data()
                );
                const newUser = await auth.createUser(fullProfile);
                await firestore
                    .collection('users')
                    .doc(newUser.uid)
                    .set(fullProfile);
                await firestore
                    .doc(`users/${sender}`)
                    .update({ invitesCount: invitesCount - 1 });
                await firestore.doc(`tmpUsers/${uid}`).delete();
                res.clearCookie('tmp_sgn');
                return getUserToken(newUser.uid, res);
            } catch (error) {
                return res.redirect(`${process.env.BASE_URL}/403`);
            }
        }
    }
);

router.post('/api/postItem', upload.array('photos', 5), (req, res) => {
    const {
        user, title, description, category, user_name, user_id
    } = req.body;
    const items = firestore.collection(`items`);
    const item = {
        title,
        description,
        category,
        status: 'active',
        rating: null,
        reviewsCount: 0,
        owner: user,
        images: req.files.map(image => ({
            url: image.url,
            thumb: image.eager[0].url
        })),
        created: Date.now()
    };
    items
        .add(item)
        .then((snapshot) => {
            const id = snapshot.id;
            const sendData = { id };
            const item = {
                id, title, description, type: 'item'
            };
            const user = { name: user_name, id: user_id };
            const text = getBotMessage({ item, user });
            axios.post(`https://communabot.herokuapp.com/sendMessage`, {
                text
            });
            res.json(sendData);
        })
        .catch(err => res.status(500).send(err));
});

router.post('/api/postRequest', upload.array('photos', 5), (req, res) => {
    const {
        user, title, description, category, user_name, user_id
    } = req.body;
    const requests = firestore.collection(`requests`);
    const request = {
        title,
        description,
        category,
        from: user,
        date: Date.now(),
        type: 'search',
        images: req.files.map(image => ({
            url: image.url,
            thumb: image.eager[0].url
        })),
        created: Date.now()
    };
    requests
        .add(request)
        .then((snapshot) => {
            const id = snapshot.id;
            const sendData = { id };
            const item = {
                id, title, description, type: 'request'
            };
            const user = { name: user_name, id: user_id };
            const text = getBotMessage({ item, user });
            axios.post(`https://communabot.herokuapp.com/sendMessage`, {
                text
            });
            res.json(sendData);
        })
        .catch(err => res.status(500).send(err));
});

router.post('/api/sendInvite', async(req, res) => {
    try {
        const { email, sender } = req.body;
        const invite = jwt.sign({ sender, email }, process.env.JWT_SECRET);
        const snap = await firestore.doc(`users/${sender}`).get();
        await transporter.sendMail({
            from: '"Почта Коммуны" communapost@yandex.ru',
            to: email,
            subject: 'Приглашение',
            html: `<h3>Привет!</h3>
            <p>Ваш друг <strong>${
    snap.data().displayName
}</strong> приглашает вас присоединится к <i>Проекту Общих Вещей <strong>Коммуна</strong></i>.<br>
            Для этого вам нужно перейти по ссылке <a href="http://www.communa.me/login?invite=${invite}">http://www.communa.me/login?invite=${invite}</a>.<br>
            Если вы не понимаете о чем речь - просто проигнорируйте это письмо =).</p>

            <p><i>С уважением, команда Коммуны.</i></p>`
        });
        res.end();
    } catch (err) {
        res.status(500).send("Mail isn't sent");
    }
});

router.post('/api/verifyInvite', (req, res) => {
    const { invite } = req.body;
    jwt.verify(invite, process.env.JWT_SECRET, async(err, decoded) => {
        if (err) return res.status(500).send('Problem with decoding');
        try {
            const { sender, email } = decoded;
            const snap = await firestore.doc(`users/${sender}`).get();
            const { invitesCount } = snap.data();
            const cookie = JSON.stringify({ sender, email });
            return invitesCount > 0
                ? res.cookie('tmp_sgn', cookie, { signed: true }).send('ok')
                : res.redirect(`${process.env.BASE_URL}/403`);
        } catch (error) {
            return res.status(500).send(error);
        }
    });
});

router.post('/api/sendMailVerification', async(req, res) => {
    try {
        const { email, sender } = req.body;
        const sign = jwt.sign({ email, sender }, process.env.JWT_SECRET);
        await transporter.sendMail({
            from: '"Почта Коммуны" communapost@yandex.ru',
            to: email,
            subject: 'Подтверждение почты',
            html: `<h3>Привет!</h3>
            <p>Для подтверждения почтового адреса перейдите по ссылке <a href="http://www.communa.me/api/verifyMail?token=${sign}">http://www.communa.me/api/verifyMail?token=${sign}</a>.<br>
            Если вы не понимаете о чем речь - просто проигнорируйте это письмо =).</p>

            <p><i>С уважением, команда Коммуны.</i></p>`
        });
        return res.end();
    } catch (error) {
        return res.status(500).send(error);
    }
});

router.get('/api/verifyMail', (req, res) => {
    const { token } = req.query;
    jwt.verify(token, process.env.JWT_SECRET, async(err, decoded) => {
        try {
            if (err) return res.status(500).redirect('/');
            const { email, sender } = decoded;
            await firestore.doc(`users/${sender}`).update({ email });
            return res.redirect('/');
        } catch (error) {
            return res.status(500).redirect('/');
        }
    });
});

router.post('/api/sendNotification', async(req, res) => {
    try {
        const { email, html } = req.body;
        await transporter.sendMail({
            from: '"Почта Коммуны" communapost@yandex.ru',
            to: email,
            subject: 'Оповещение',
            html
        });
        return res.end();
    } catch (error) {
        return res.status(500).send(error);
    }
});

module.exports = router;
