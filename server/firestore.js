const Firestore = require('@google-cloud/firestore');

const firestore = new Firestore({
    projectId: 'communa-project',
    keyFilename: './key.json'
});

module.exports = firestore;
