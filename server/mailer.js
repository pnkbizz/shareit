const nodemailer = require('nodemailer');

const smtpConfig = {
    service: 'Yandex',
    auth: {
        user: 'communapost',
        pass: process.env.POST_PASS
    }
};

const transporter = nodemailer.createTransport(smtpConfig);

module.exports = transporter;
