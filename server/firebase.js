const admin = require('firebase-admin');
const serviceAccount = require('../key.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://communa-project.firebaseio.com'
});

const db = admin.database();
const auth = admin.auth();

module.exports = { db, auth };
