const passport = require('passport');
const VKontakteStrategy = require('passport-vkontakte').Strategy;
const { auth } = require('./firebase');
const firestore = require('./firestore');

passport.use(
    new VKontakteStrategy(
        {
            clientID: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
            callbackURL: '/api/auth/vkontakte/callback',
            scope: ['friends', 'notify'],
            profileFields: ['city', 'photo_400_orig']
        },
        async (accessToken, refreshToken, params, profile, done) => {
            const userSnap = profile.id
                ? await firestore
                      .collection('users')
                      .where('id', '==', profile.id)
                      .get()
                : {};
            const dbUser = userSnap.docs[0] ? userSnap.docs[0].id : null;

            try {
                if (dbUser) {
                    const user = await auth.getUser(dbUser);
                    done(null, { uid: user.uid });
                } else {
                    const users = firestore.collection('tmpUsers');
                    const additionalFields = {
                        invitesCount: 0,
                        rating: null,
                        reviewsCount: 0,
                        vkId: profile.id,
                        addedBy: [],
                        searches: []
                    };
                    const fullProfile = Object.assign(
                        additionalFields,
                        profile
                    );
                    const { id } = await users.add(fullProfile);
                    done(null, { uid: id });
                }
            } catch (err) {
                done(err);
            }
        }
    )
);

passport.serializeUser((uid, done) => {
    done(null, uid);
});

passport.deserializeUser((uid, done) => {
    firestore
        .doc(`users/${uid}`)
        .get()
        .then(user => done(null, user.data()))
        .catch(done);
});

module.exports = passport;
